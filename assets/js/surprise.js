// Browser detection
let browserName = ""

const is_explorer= typeof document !== 'undefined' && !!document.documentMode && !isEdge;
const is_firefox = typeof window.InstallTrigger !== 'undefined';
const is_safari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
const is_opera = !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
const is_chrome = !!window.chrome && !is_opera;

function is_touch_device() {
  return (('ontouchstart' in window)
       || (navigator.MaxTouchPoints > 0)
       || (navigator.msMaxTouchPoints > 0));
}

if (is_firefox) {
  browserName = "firefox"
} else if (is_chrome) {
  browserName = "chrome"
}


const inputs = document.querySelectorAll(".fake-input")
// Question inputs by ID
const nameInput = document.getElementById("name");
const mindsetInput = document.getElementById("mindset");
const lookingForInput = document.getElementById("looking-for");
const bondInput = document.getElementById("bond");
const acceptanceInput = document.getElementById("acceptance");
const cooperationInput = document.getElementById("cooperation");
// print selectors
const imgWrapper = document.getElementById("print-image-wrapper")
const imgElement = document.getElementById("image-element") 
const imgOriginal = document.getElementById("image-original") 

// Timers to get time it took the user to answer
let timer = 0
let timerQ1 = 0
let timerQ2 = 0
let timerQ3 = 0
let timerQ4 = 0
let timerQ5 = 0
let timerQ6 = 0


// Random fn
function randomBooleanWithChance(chance) {
  const random = Math.random();
  let value = false;
  if (random <= chance) {
    value = true;
  } 
  return value;
}

function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}

// Global timer incrementing every second
function startTimer() {
  setInterval(function() {
    timer++;
  }, 1000);
}


const colors = ["red", "orange", "yellow", "green", "blue", "indigo", "violet"]

function randomColor() {
  const int = getRandomInt(7)
  return colors[int]
}

inputs.forEach(input => {
  const color = randomColor();
  input.classList.add(color)
});

// Insert new line in terminal
const terminal = document.getElementById("terminal-content")

function scrollTerminal() {
  terminal.scrollTop = terminal.scrollHeight;
}

function addTerminalEntry(id, name, callback, color) {
  const entryColor = color ? color : randomColor()
  const entry = name ? 
                  `<div class="terminal-entry ${entryColor}" id="${id}"><span class="terminal-entry-name-section">${name}@${browserName}:</span><div id="${id}-content" class="terminal-entry-text"></div></div>`:
                  `<div class="terminal-entry ${entryColor}" id="${id}"><span class="terminal-entry-name-section"></span><div id="${id}-content" class="terminal-entry-text"></div></div>`;
  terminal.insertAdjacentHTML('beforeend', entry);
  scrollTerminal();
  if (callback && typeof(callback) === "function") {
    callback();
  }
}

let talkSpeed = 60 // in ms
let debugMode = false

function animateText(array, element, hint, hintElement, callback) {
  var fn = setInterval(() => {
    if (array.length > 0) {
      hint ? 
        hintElement.insertAdjacentHTML('beforeend', array[0]):
        element.insertAdjacentHTML('beforeend', array[0]);
      array.shift();
      startTalking();
      scrollTerminal();
    } else {
      clearInterval(fn);
      stopTalking();
      if (callback && typeof(callback) === "function") {
        callback();
      }
    }
  }, debugMode ? 0 : talkSpeed);
}

function displayUncompatibleBrowser() {

}

// Display text
function displayText(elementId, text, animated, callback, hint) {
  const element = document.getElementById(elementId+"-content")
  if (hint) {
    element.insertAdjacentHTML('beforeend', `<div id="${elementId}-hint" class="hint"></div>`);
  }
  const hintElement = document.getElementById(elementId+"-hint")
  let arrayFromText = text.split("") 
  if (animated) {
    animateText(arrayFromText, element, hint, hintElement, callback)
    // var fn = setInterval(() => {
    //   if (arrayFromText.length > 0) {
    //     hint ? 
    //       hintElement.insertAdjacentHTML('beforeend', arrayFromText[0]):
    //       element.insertAdjacentHTML('beforeend', arrayFromText[0]);
    //     arrayFromText.shift();
    //     startTalking();
    //     scrollTerminal();
    //   } else {
    //     clearInterval(fn);
    //     stopTalking();
    //     if (callback && typeof(callback) === "function") {
    //       callback();
    //     }
    //   }
    // }, debugMode ? 0 : talkSpeed);
  } else {
    hint ? 
      hintElement.insertAdjacentHTML('beforeend', `[${text}]`):
      element.insertAdjacentHTML('beforeend', text);
      scrollTerminal();
    if (callback && typeof(callback) === "function") {
      callback();
    }
  }
}

function displayColoredText(elementId, text, color, animated, callback) {
  const element = document.getElementById(elementId+"-content")
  const colorElementId = `${elementId}-content-colored-${color}`
  element.insertAdjacentHTML('beforeend', `<span id="${colorElementId}" class="paper-color-instruction ${color}"></span>`)
  let arrayFromText = text.split("") 
  if (animated) {
    var fn = setInterval(() => {
      const coloredElement = document.getElementById(colorElementId)
      if (arrayFromText.length > 0) {
        coloredElement.insertAdjacentHTML('beforeend', arrayFromText[0])
        arrayFromText.shift();
        startTalking();
        scrollTerminal();
      } else {
        clearInterval(fn);
        stopTalking();
        if (callback && typeof(callback) === "function") {
          callback();
        }
      }
    }, debugMode ? 0 : talkSpeed);
  } else {
    const coloredElement = document.getElementById(colorElementId)
    coloredElement.insertAdjacentHTML('beforeend', text)
    scrollTerminal();
    if (callback && typeof(callback) === "function") {
      callback();
    }
  }
}

function addTerminalEntryWithText(id, name, text, animated, callback, timeout) {
  const timeoutDuration = timeout ? timeout : 0; 
  addTerminalEntry(id, name, function() {
    displayText(id, text, animated, function() {
      setTimeout(() => {
        if (callback && typeof(callback) === "function") {
          callback();
        }
      }, debugMode ? 0 : timeoutDuration);
    })
  })
}

const listTalkSpeed = 140

function displayAnswerList(id, width, answers, callback) {
  const contentElement = document.getElementById(id+"-content")
  contentElement.classList.add("list-wrapper")
  let iteration = 0
  var fn = setInterval(() => {
    if (iteration < answers.length) {
      const entryElement = `<div style="width:${width}%;">
                            ${answers[iteration]}
                          </div>`
      contentElement.insertAdjacentHTML('beforeend', entryElement);
      startTalking();
      scrollTerminal();
      iteration++
    } else {
      clearInterval(fn);
      stopTalking();
      if (callback && typeof(callback) === "function") {
        callback();
      }
    }
  }, debugMode ? 0 : listTalkSpeed);
}



// Crippy
const crippyName = "Crippy" 
const mouthCharacters = ["−", "=", "•", "÷"];
const openedEye = "°"
const closedEye = "^"
const crippy = document.getElementById("crippy")
const eyeLeft = document.getElementById("eye-left")
const mouth = document.getElementById("mouth")
const eyeRight = document.getElementById("eye-right")

function openEyes() {
  eyeLeft.innerHTML = openedEye
  eyeRight.innerHTML = openedEye
}
function closeEyes() {
  eyeLeft.innerHTML = closedEye
  eyeRight.innerHTML = closedEye
}

function blink(chance) {
  shouldBlink = randomBooleanWithChance(chance);
  if (shouldBlink) {
    closeEyes();
    setTimeout(function() {
      openEyes();
    }, 200)
  }
}

let shouldBeBlinking = true

function randomBlink() {
  var fn = setInterval(() => {
    if (shouldBeBlinking) {
      blink(0.3);
      // console.log("blink")
    } else {
      clearInterval(fn);
      // console.log("no blink")
    }
  }, 800);
}

function startThinking(callback, color) {
  // const color = randomColor()
  mouth.innerHTML = "~";
  eyeLeft.innerHTML = "¨";
  eyeRight.innerHTML = "¨";
  shouldBeBlinking = false
  mouth.classList.add("thinking-mouth")
  crippy.classList.add("is-thinking", color)
  if (callback && typeof(callback) === "function") {
    callback();
  }
}

function stopThinking(callback) {
  mouth.innerHTML = "−";
  openEyes();
  shouldBeBlinking = true;
  mouth.classList.remove("thinking-mouth")
  crippy.classList.remove("is-thinking")
  colors.forEach(color => {
    crippy.classList.remove(color)
  });
  if (callback && typeof(callback) === "function") {
    callback();
  }
  randomBlink();
}

function animateLoader(id, duration) {
  element = document.getElementById(id)
  let counter = 0
  var fn = setInterval(() => {
    if (counter == 0) {
      element.innerHTML = "…\\"
    } else if (counter == 1) {
      element.innerHTML = "…|"
    } else if (counter == 2) {
      element.innerHTML = "…/"
    } else if (counter == 3) {
      element.innerHTML = "…−"
      counter = 0
    }
    counter++
  }, 200);
  setTimeout(() => {
    clearInterval(fn);
    element.innerHTML = "…done"
  }, duration);
}

function think(id, duration, callback, text) {
  const thinkDuration = debugMode ? 0 : duration
  const displayedText = text ? text : "thinking";
  const thinkingColor = randomColor();
  startThinking(function(){
    addTerminalEntry(id, crippyName, function(){
      const element = document.getElementById(id)

      element.insertAdjacentHTML(
        'beforeend', 
        `<span id="${id}-text">${displayedText}</span><span id="${id}-loader"></span>`
      );
      document.getElementById(id+"-text").classList.add("thinking")
      document.getElementById(id+"-loader").classList.add("thinking")
      animateLoader(id+"-loader", thinkDuration);
    }, thinkingColor)
    setTimeout(() => {
      stopThinking(callback)
    }, thinkDuration);
  }, thinkingColor)
}



function startTalking() {
  const randomMouthInt = getRandomInt(3);
  mouth.innerHTML = mouthCharacters[randomMouthInt]
  blink(0.1);
}

function stopTalking() {
  mouth.innerHTML = mouthCharacters[0]
}


// INPUT FOCUS

// Set cursor to the end of the div when focusing it
// comes from https://stackoverflow.com/questions/1125292/how-to-move-cursor-to-end-of-contenteditable-entity
function setEndOfContenteditable(contentEditableElement) {
  var range,selection;
  range = document.createRange(); //Create a range (a range is a like the selection but invisible)
  range.selectNodeContents(contentEditableElement); //Select the entire contents of the element with the range
  range.collapse(false); //collapse the range to the end point. false means collapse to end rather than the start
  selection = window.getSelection(); //get the selection object (allows you to change selection)
  selection.removeAllRanges(); //remove any selections already made
  selection.addRange(range); //make the range you have just created the visible selection
}


// focus the fake input when clicking it
// add the classes to display underscore fake caret 
inputs.forEach(inputItem => {
  inputItem.onclick = function(e) {
    e.stopPropagation();
    // const elementToFocus = inputItem.childNodes[1];
    setEndOfContenteditable(inputItem);
    inputItem.focus();
    inputItem.classList.add("focused");
  }
});

document.addEventListener("click", () => {
  const focusedInput = document.querySelector(".focused")
  if (focusedInput) {
    focusedInput.classList.remove("focused");
  }
})

function preventCursorMovement(e) {
  if ((e.key == "ArrowRight") || (e.key == "ArrowLeft")) {
    e.preventDefault();
  }
}


function focusElement(element) {
  element.classList.add("active");
  element.focus();
  const currentFocus = document.activeElement
  if (document.hasFocus()) {
    currentFocus.classList.add("focused");
  }
}

function unfocusElement(element) {
  element.classList.remove("active", "focused");
}

document.addEventListener('focusout', function() {
  const activeInput = document.querySelector(".active");
  if (activeInput) {
    activeInput.classList.remove("focused");
  }
}, true);

function throwError(name, input, count, hint) {
  const answer = input.innerHTML
  input.innerHTML = ""
  const errorId = count.value
  count.value++   
  if (count.value > 4) {
    input.classList.remove("active", "focused");
      addTerminalEntryWithText(`${name}-not-serious`, crippyName, "Too many errors, you are not taking this seriously", true, function() {
        addTerminalEntryWithText(`${name}-bye`, crippyName, "Bye", true, null)
      })
  } else {
    addTerminalEntryWithText(`${name}-error-feedback`+errorId, userName, `<span class="line-through">${answer}</span>`, false, function() {
      addTerminalEntryWithText(`${name}-value-error`+errorId, crippyName, "The value you entered does not match", true, function() {
        displayText(`${name}-value-error`+errorId, hint, false, null, true)
      })
    })
  }
}

// TIMELINE 

randomBlink();

// QUESTIONS
// Data
let userName = "" 
let nameHash = ""
let imgId = "" // username hash
let imageFileName = ""

let userPing = 0
let userIP = ""

let translateX = 0 // based on userName length
let translateY = 0 // based on userName length
let rotate = [0, 0, 0] // if q2 chose 1
let scale = 100 // if q2 chose 2
let skewX = 0 // if q2 chose 5
let skewY = 0 // if q2 chose 3

let transformValue = ""

let mixBlendMode = "" //if q2 chose 6
let blendColor = "" // defined with name hash 

let shouldBlur = false
let blur = "" // if q2 chose 4
let shouldHue = false
let hue = "" //  if q6 is 1, use namehash % 360
let shouldInvert = false
let invert = "" // if q6 is 2, use q4 percentage 
let shouldGreyscale = false
let greyscale = "" // if q6 is 3, use q4 percentage 
let shouldContrast = false
let contrast = "" // if q5 true, use q4 percentage*10

let paperColor = ""

var p = new Ping();

p.ping("https://print-it.objetpapier.fr", function(err, data) {
  if (err) {
    userPing = -data
  } else {
    userPing = data
  }
  // console.log(userPing)
});

function json(url) {
  return fetch(url).then(res => res.json());
}

let apiKey = '2fc0dbe0c5b4a60c67c66eba3bcaadd37fcd4eb05e75e98debfa66ae';
json(`https://api.ipdata.co?api-key=${apiKey}`).then(data => {
  userIP = data.ip
  // console.log(data.city);
  // console.log(data.country_code);
  // so many more properties
});

// json('https://api.wheretheiss.at/v1/satellites/25544').then(data => {
//   console.log(data)
// })


// Question 1
function hashCode(str) { // java String#hashCode
  var hash = 0;
  for (var i = 0; i < str.length; i++) {
    hash = str.charCodeAt(i) + ((hash << 5) - hash);
  }
  return hash;
} 

function intToRGB(i){
  var c = (i & 0x00FFFFFF)
      .toString(16)
      .toUpperCase();

  return "00000".substring(0, 6 - c.length) + c;
}

const supriseTxtAnim = document.querySelectorAll(".surprise-animation")

supriseTxtAnim.forEach(element => {
  const content = element.innerHTML;
  const textArray = content.split("")
  element.innerHTML = ""
  textArray.forEach(letter => {
    element.insertAdjacentHTML('beforeend', `<span class="animated-letter">${letter}</span>`)
  });
});



const startButton = document.getElementById("start-button")
const intro = document.querySelector(".greeting");
const terminalWrapper = document.querySelector(".surprise-terminal-wrapper")
const surpriseWrapper = document.querySelector(".surprise-wrapper")

function start() {
  surpriseWrapper.classList.remove("idle");
  startTimer();
  intro.classList.add("slide-out")
  setTimeout(() => {
    if (is_firefox || is_chrome) {
      intro.classList.add("hidden")
      terminalWrapper.classList.add("displayed")
      setTimeout(() => {
        terminalWrapper.classList.remove("animated")
        startConversation();
      }, 2500)
    } else {
      displayUncompatibleBrowser();
    }
  }, 1500)
}

startButton.onclick = function() {
  start();
}

document.onkeydown = function(e) {
  if (surpriseWrapper.classList.contains("idle")) {
    if (e.key == "Enter") {
      start();
    }
  } 

  if(e.ctrlKey && e.key == "\\"){
    debugMode = true;
  } 
}

function startConversation() {
  addTerminalEntryWithText("hello-prompt", crippyName, "Hello", true, displayFirstQuestion, 500)
}

const nameQuestion = "I'm Crippy, how should I call you?"
const nameQuestionForPrint = "What is your name?"

function displayFirstQuestion(){
  addTerminalEntryWithText("name-prompt", crippyName, nameQuestion, true, function() {
    // nameInput.classList.add("focused", "active");
    // nameInput.focus();
    focusElement(nameInput);
  })
}


nameInput.onkeydown = function(e){
  preventCursorMovement(e)
  if ((e.key == "Enter") && nameInput.innerHTML.length < 1) {
    e.preventDefault()
    nameInput.innerHTML = ""
  } else if (e.key == "Enter"){
    e.preventDefault();
    userName = nameInput.innerHTML;

    nameHash = hashCode(userName);
    blendColor = intToRGB(nameHash);

    // Select Image
    imgId = Math.abs(nameHash % 236);
    timerQ1 = timer;
    if (imgId.toString().length == 1) {
      imageFileName = "0000" + imgId + ".jpg"
    } else if (imgId.toString().length == 2) {
      imageFileName = "000" + imgId + ".jpg"
    } else {
      imageFileName = "00" + imgId + ".jpg"
    }
    
    const imgPath = imgElement.getAttribute("src")
    const imgPathWithFile = imgPath + imageFileName; // "07.jpg" 
    imgElement.setAttribute("src", imgPathWithFile)
    imgOriginal.setAttribute("src", imgPathWithFile)
    imgWrapper.style.backgroundColor = "#" + blendColor

    // Set translate
    const currentdate = new Date(); 
    const nameLengthModulo = userName.length % 10
    const isSecondEven = currentdate.getSeconds() % 2 == 0
    const isMinuteEven = currentdate.getMinutes() % 2 == 0
    
    if (nameLengthModulo > 3 && 5 >= nameLengthModulo) {
      translateX = isSecondEven ? currentdate.getSeconds() : -currentdate.getSeconds();
    } else if (nameLengthModulo > 5 && 7 >= nameLengthModulo) {
      translateY = isSecondEven ? currentdate.getSeconds() : -currentdate.getSeconds();
    } else if (nameLengthModulo > 7) {
      translateX = isMinuteEven ? currentdate.getMinutes() : -currentdate.getMinutes();
      translateY = isSecondEven ? currentdate.getSeconds() : -currentdate.getSeconds();
    }

    // console.log(translateX, translateY)

    // Complete the verso page
    const crippyNamePrint = document.querySelectorAll(".crippy-name-print")
    crippyNamePrint.forEach(nameElement => {
      nameElement.innerHTML = crippyName + "@" + browserName + ":"
      nameElement.classList.add(randomColor());
    });

    const userNamePrint = document.querySelectorAll(".user-name-print")
    userNamePrint.forEach(nameElement => {
      nameElement.innerHTML = userName + "@" + browserName + ":"
      nameElement.classList.add(randomColor());
    });
    const questionNamePrint = document.querySelector(".question-name-print")
    questionNamePrint.innerHTML = nameQuestionForPrint;

    const answerNamePrint = document.querySelector(".answer-name-print")
    answerNamePrint.innerHTML = userName;
    

    unfocusElement(nameInput)
    addTerminalEntryWithText("user-name-prompt", null, userName, false, thinkAboutName)
  } 
}



function thinkAboutName() {
  think("name-thinking", 1000, displayMindsetPrompts)
}

function setImageRatio() {
  const imageHeight = imgElement.height;
  const imageWidth = imgElement.width;
  // console.log(imageHeight, imageWidth)
  if (imageHeight >= imageWidth) {
    imgWrapper.classList.add("vertical")
  }
}

const mindsetGreeting = "Nice to meet you, "
const mindsetIntro = "I have a few questions for you"
const mindsetQuestion = "What is/are your state of mind when surfing the web?"

function displayMindsetPrompts() {
  addTerminalEntryWithText("hello-to-user-prompt", crippyName, mindsetGreeting + userName, true, function() {
    setImageRatio();
    addTerminalEntryWithText("a-few-questions-prompt", crippyName, mindsetIntro, true, function() {
      addTerminalEntryWithText("mindset-question-prompt", crippyName, mindsetQuestion, true, displayMindsetAnswers)
    }, 500)   
  }, 500)
}

const mindsetAnswers = ["1-🤣","2-🧐","3-😡","4-🥴","5-😈","6-😑"]

function displayMindsetAnswers() {
  displayAnswerList("mindset-question-prompt", 33, mindsetAnswers, function(){
    displayText("mindset-question-prompt", "Separate numbers with spaces", false, function() {
      focusElement(mindsetInput)
    }, true)
  })
}


let mindsetErrorCount = {value: 0}

function testArray(array, condition) {
  let verification = array.some( ai => condition.includes(ai) );
  return verification
}

mindsetInput.onkeydown = function(e){
  preventCursorMovement(e)
  // if (e.keyCode > 32 && (e.keyCode < 48 || e.keyCode > 57)){
  //   e.preventDefault();
  // } else 
  if (e.key == "Enter" && mindsetInput.innerHTML.length < 1) {
    e.preventDefault()
    mindsetInput.innerHTML = ""
  } else if (e.key == "Enter") {
    e.preventDefault();
    timerQ2 = timer;
    const currentDate = new Date(); 
    const mindset = mindsetInput.innerHTML;
    const mindsetArray = mindset.split(" ");
    const mindsetArrayInt = mindset.split(" ").map(function(int) {
      return parseInt(int);
    })
    
    if (testArray(mindsetArrayInt, [1,2,3,4,5,6])) {

      if (mindsetArrayInt.includes(1)) {
        const rotateX = timerQ2 % 2 == 0 ? (timerQ2*userName.length)%60 : 0;
        const rotateY = timerQ1 % 2 != 0 ? ((timerQ2-timerQ1)*userName.length)%60 : 0;
        const rotateZ = userPing % 2 == 0 ? userPing : 0;
        rotate = [rotateX, rotateY, rotateZ]
        
        // console.log("rotate", rotate)
      }

      if (mindsetArrayInt.includes(2)) {
        scaleMultiplier = currentDate.getSeconds() % 2 == 0 ? 1 : 10
        scale = timerQ2 % 2 == 0 ? timerQ2*scaleMultiplier : -timerQ2*scaleMultiplier;
        // console.log("scale", scale)
      }

      if (mindsetArrayInt.includes(3)) {
        skewY = (timerQ2*(currentDate.getSeconds()/2))%80;
        // console.log("skewY", skewY)
      }

      if (mindsetArrayInt.includes(4)) {
        shouldBlur = true
        blur = `blur(${userPing}px)`;
        // console.log("blur", blur)
      }

      if (mindsetArrayInt.includes(5)) {
        skewX = (timerQ2*currentDate.getSeconds())%80;
        // console.log("skewX", skewX)
      }

      if (mindsetArrayInt.includes(6)) {
        const modes = ["multiply", "exclusion", "color"]
        const modeSelection = timerQ2 % 3;
        mixBlendMode = modes[modeSelection];
        imgElement.style.mixBlendMode = mixBlendMode
      }

      let mindsetFeedbackArray = []
      mindsetArray.forEach(mindsetEntry => {
        const mindsetInt = parseInt(mindsetEntry)
        if (isNaN(mindsetInt) || mindsetInt > 6) {
          mindsetFeedbackArray.push(`<span class="line-through mgr-s">${mindsetEntry}</span>`)
        } else {
          mindsetFeedbackArray.push(`<span class="mgr-s">${mindsetAnswers[mindsetInt - 1]}</span>`)
        }
      });
      const mindsetFeedback = mindsetFeedbackArray.join(" ")

//rotate3d(${rotate[0]}deg,${rotate[1]}deg,${rotate[2]}deg)   
      transformValue = `translate(${translateX}%, ${translateY}%) 
                        scale(${scale}%) 
                        skewX(${skewX}deg) 
                        skewY(${skewY}deg) 
                        rotateX(${rotate[0]}deg) 
                        rotateY(${rotate[1]}deg) 
                        rotateZ(${rotate[2]}deg)`
      // console.log(transformValue)
      imgWrapper.style.transform = transformValue

      unfocusElement(mindsetInput)
      addTerminalEntryWithText("mindset-answer-prompt", userName, mindsetFeedback, false, thinkAboutMindset)

      const questionMindsetPrint = document.querySelector(".question-mindset-print")
      questionMindsetPrint.innerHTML = mindsetQuestion;
      const answerMindsetPrint = document.querySelector(".answer-mindset-print")
      answerMindsetPrint.innerHTML = mindsetFeedback;
    } else {
      throwError("mindset", mindsetInput, mindsetErrorCount,"Separate numbers with spaces")
    }

  } 
}

function thinkAboutMindset() {
  think("mindset-thinking", 3000, displayLookingFor)
}


const lookingForQuestion = "What would you expect to learn from the internet?";
const lookingForAnswers = ["1-How to become rich?",
                           "2-How to cure a rare disease?",
                           "3-How to find love?",
                           "4-How to become a famous influencer?",
                           "5-How to delete your browser history?"]

function displayLookingFor() {
  addTerminalEntryWithText("looking-for-question-prompt", crippyName, lookingForQuestion, true, function() {
    displayAnswerList("looking-for-question-prompt", 100, lookingForAnswers, function(){
      displayText("looking-for-question-prompt", "number", false, function() {
        focusElement(lookingForInput)
      }, true)
    })
  })
}

let lookingForErrorCount = {value: 0}

lookingForInput.onkeydown = function(e) {
  preventCursorMovement(e);
  if (e.key == "Enter" && mindsetInput.innerHTML.length < 1) {
    e.preventDefault()
    mindsetInput.innerHTML = ""
  } else if (e.key == "Enter") {
    e.preventDefault();
    const lookingForValue = lookingForInput.innerHTML;
    const lookingForValueInt = parseInt(lookingForValue)
    if (isNaN(lookingForValueInt) || lookingForValueInt > 5 || lookingForValueInt < 1) {
      throwError("looking-for", lookingForInput, lookingForErrorCount,"number")
    } else {
      if (lookingForValueInt == 1) { paperColor = "orange"}
      else if (lookingForValueInt == 2) { paperColor = "yellow"}
      else if (lookingForValueInt == 3) { paperColor = "green"}
      else if (lookingForValueInt == 4) { paperColor = "blue"}
      else if (lookingForValueInt == 5) { paperColor = "violet"}
      const lookingForFeedback = lookingForAnswers[lookingForValueInt - 1]
      // console.log(lookingForAnswers, lookingForValueInt)
      unfocusElement(lookingForInput)
      addTerminalEntryWithText("looking-for-answer-prompt", userName, lookingForFeedback, false, thinkAboutLookingFor)

      const questionLookingForPrint = document.querySelector(".question-looking-for-print")
      questionLookingForPrint.innerHTML = lookingForQuestion;
      const answerLookingForPrint = document.querySelector(".answer-looking-for-print")
      answerLookingForPrint.innerHTML = lookingForFeedback;
    }
    
  }
}

function thinkAboutLookingFor() {
  think("looking-for-thinking", 3000, displayBond)
}

let bond = 0

const bondQuestion = "How much are you impregnated with the digital network?"

function displayBond() {
  addTerminalEntryWithText("ask-about-bond-prompt", crippyName, bondQuestion, true, function() {
    displayText("ask-about-bond-prompt", "percentage", false, function() {
      focusElement(bondInput)
    }, true)
  })
}

let bondErrorCount = {value: 0}

bondInput.onkeydown = function(e){
  preventCursorMovement(e)
  if (e.key == "Enter" && bondInput.innerHTML.length < 1) {
    e.preventDefault()
    bondInput.innerHTML = ""
  } else if (e.key == "Enter") {
    // console.log(bondInput.innerHTML.length)
    e.preventDefault();
    const bondPercentage = bondInput.innerHTML;
    const bondPercentageInt = parseInt(bondPercentage.replace('%',''))
    if (isNaN(bondPercentageInt)) {
      throwError("bond", bondInput, bondErrorCount, "percentage")
    } else {
      // console.log(bondPercentageInt)
      bond = bondPercentageInt
      bondFeedback = bondPercentageInt + "%"
      unfocusElement(bondInput)
      addTerminalEntryWithText("bond-answer-feedback", userName, bondFeedback, false, thinkAboutBond)

      const questionBondPrint = document.querySelector(".question-bond-print")
      questionBondPrint.innerHTML = bondQuestion;
      const answerBondPrint = document.querySelector(".answer-bond-print")
      answerBondPrint.innerHTML = bondFeedback;
    }
  }
}

function thinkAboutBond() {
  think("bond-thinking", 2000, displayAcceptance)
}

const acceptanceQuestion = "In your opinion, is human society ready to give way to a computerized way of living?"

function displayAcceptance() {
  addTerminalEntryWithText("ask-about-acceptance-prompt", crippyName, acceptanceQuestion, true, function() {
    displayText("ask-about-acceptance-prompt", "y/n", false, function() {
      focusElement(acceptanceInput)
    }, true)
  })
}


let acceptanceErrorCount = {value: 0}

acceptanceInput.onkeydown = function(e){
  preventCursorMovement(e)
  // if ((acceptanceInput.innerHTML.length > 0) && ((e.keyCode > 47 && e.keyCode < 58) || (e.keyCode > 64 && e.keyCode < 91))) {
  //   e.preventDefault();
  // } else if ((e.keyCode > 47 && e.keyCode < 58) || (e.keyCode > 64 && e.keyCode < 78) || (e.keyCode > 78 && e.keyCode < 89) || e.keyCode == 90) {
  //   e.preventDefault();
  // }
  if (e.key == "Enter" && acceptanceInput.innerHTML.length < 1) {
    e.preventDefault()
    acceptanceInput.innerHTML = ""
  } else if (e.key == "Enter") {
    e.preventDefault();
    const acceptance = acceptanceInput.innerHTML;
    if ((acceptance.includes("y") || acceptance.includes("n") || acceptance.includes("Y") || acceptance.includes("N")) && acceptance.length < 2) {
      if (acceptance.includes("y") || acceptance.includes("Y")) {
        shouldContrast = true
      } 
      unfocusElement(acceptanceInput)
      addTerminalEntryWithText("acceptance-answer-feedback", userName, acceptance, false, thinkAboutAcceptance)

      const questionAcceptancePrint = document.querySelector(".question-acceptance-print")
      questionAcceptancePrint.innerHTML = acceptanceQuestion;
      const answerAcceptancePrint = document.querySelector(".answer-acceptance-print")
      answerAcceptancePrint.innerHTML = acceptance;
    } else {
      throwError("acceptance", acceptanceInput, acceptanceErrorCount, "y/n")
    }
  }
}

function thinkAboutAcceptance() {
  think("acceptance-thinking", 1500, displayCooperation)
}

const cooperationQuestion = "Are you in favor of cooperation with the machine?"
const cooperationAnswers = ["1-👍","2-👎","3-🤷"]
const cooperationHint = "number"

function displayCooperation() {
  addTerminalEntryWithText("cooperation-question-prompt", crippyName, cooperationQuestion, true, function() {
    displayAnswerList("cooperation-question-prompt", 33, cooperationAnswers, function() {
      displayText("cooperation-question-prompt", cooperationHint, false, function() {
        focusElement(cooperationInput)
      }, true)
    })
  })
}

let cooperationErrorCount = {value: 0}

cooperationInput.onkeydown = function(e) {
  preventCursorMovement(e)
  if (e.key == "Enter" && cooperationInput.innerHTML.length < 1) {
    e.preventDefault()
    cooperationInput.innerHTML = ""
  } else if (e.key == "Enter") {
    e.preventDefault();
    const cooperation = cooperationInput.innerHTML;
    const cooperationInt = parseInt(cooperation)
    const cooperationFeedback = cooperationAnswers[cooperationInt - 1]
    if (isNaN(cooperationInt) || cooperationInt > 3 || cooperationInt < 1) {
      throwError("cooperation", cooperationInput, cooperationErrorCount, cooperationHint)
    } else {
      if (cooperationInt == 1) {
        shouldHue = true
      } else if (cooperationInt == 2) {
        shouldInvert = true
      } else if (cooperationInt == 3) {
        shouldGreyscale = true
      }
      unfocusElement(cooperationInput)
      addTerminalEntryWithText("cooperation-answer-feedback", userName, cooperationFeedback, false, addFilter)

      const questionCooperationPrint = document.querySelector(".question-cooperation-print")
      questionCooperationPrint.innerHTML = cooperationQuestion;
      const answerCooperationPrint = document.querySelector(".answer-cooperation-print")
      answerCooperationPrint.innerHTML = cooperationFeedback;
    }
  }
}

function addFilter() {
  if (shouldContrast) {
    contrast = `contrast(${bond*10}%)`
  }
  if (shouldHue) {
    hue = `hue-rotate(${nameHash%360}deg)`
  }
  if (shouldGreyscale) {
    greyscale = `greyscale(${bond}%)`
  }
  if (shouldInvert) {
    invert = `invert(${bond}%)`
  }
  const filterString = `${blur} ${invert} ${greyscale} ${hue} ${contrast}`
  imgElement.style.filter = filterString

  thinkAboutCooperation();
}

function thinkAboutCooperation() {
  

  think("cooperation-thinking", 2500, displayPrintAdvices)
}

const thankPrompt = "Thank you for this conversation"
const computePrompt = "I will now compute a representation of your digital self based on your answers"

function displayPrintAdvices() {
  addTerminalEntryWithText("thanks-prompt", crippyName, thankPrompt, true, function() {
    addTerminalEntryWithText("compute-prompt", crippyName, computePrompt, true, function() {
      computePortrait();
    }, 500)  
  }, 500)
}

function computePortrait() {
  think("compute-thinking", 4000, displayPrintInstructions, "computing")
}

// add paper selection
// change instruction to recto verso
const printInstructions = `Ready for printing, `

const firefoxInstructions = [`-select landscape orientation`,
                             `-set paper size to A4`,
                             `-set scale to fit the page`,
                             `-use default margins`,
                             `-check the print background checkbox`,
                             `-select two-sided printing`]
const chromeInstructions = [`- set paper size to A4`,
                            `- use default margins`,
                            `- use default scale`,
                            `- check the background graphics checkbox`]

const helpPrompt = `Please refer to my humanoid assciates @objet_papier to setup the printing options`

let printCounter = 0

function displayPrintInstructions() {

  const paperColorSpan = `<span class="paper-color-instruction ${paperColor}">${paperColor}</span>`
  const paperInstruction = `You should use ${paperColorSpan} colored paper and print 5 copies`
  


  addTerminalEntryWithText("print-instructions", crippyName, printInstructions, true, function(){
    displayText("print-instructions", "you should use ", true, function() {
      displayColoredText("print-instructions", paperColor, paperColor, true, function() {
        displayText("print-instructions", " paper", true, function() {
          addTerminalEntryWithText("help-prompt", crippyName, helpPrompt, true, function(){
            addTerminalEntryWithText("press-p", crippyName, "Press P to print", true, function(){
              document.onkeydown = function(e) {
                if (e.key == "p") {
                  think(`print-thinking-${printCounter}`, 3000, function(){
                    window.print();
                    printCounter++
                  }, "launchingPrintModal")
                }
              }
            })
          })
        })
      } )
    })
    // addTerminalEntryWithText("paper-instructions", crippyName, paperInstruction, true, function(){
      
    // }, 500)
  }, 500)
}

