---
image: "/assets/images/grrrls/lauren-mauffatt.gif"
artist: "Lauren Mauffatt"
bio: "Lauren Moffatt est une artiste australienne qui travaille avec les médias immersifs. Elle explore les sources de friction entre les mondes virtuels et physiques."
website: "deptique.net"
artwork:
  - title: "The Unbinding"
    format: "2014 - Installation vidéo stéréoscopique"
    meta: "Une production Le Fresnoy, Studio national des arts contemporains"
    desc: >-
      Un visage fragmenté dans une cabane au milieu d'une forêt qui flirte avec la vallée dérangeante. Dans son œuvre, Lauren Moffatt fait référence aux mots de l’artiste Hito Steyerl selon laquelle le cerveau fonctionne comme un navigateur Web où chaque image en rappelle une autre.
---