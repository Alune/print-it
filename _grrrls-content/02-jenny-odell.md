---
image: "/assets/images/grrrls/jenny-odell.gif"
artist: "Jenny Odell"
bio: "Jenny Odell est une artiste et autrice vivant à Oakland en Californie. Son travail est souvent en lien avec les archives. Elle est l’autrice d’un livre How to do nothing, Resisting the Attention Economy, à paraître en avril 2019."
website: "www.jennyodell.com"
artwork: 
  - title: "Polly Returns"
    format: "2017 - Installation vidéo stéréoscopique"
    meta:  "Courtesy de l’artiste"
    desc: >-
      Jenny Odell s'inspire d'une animation 3D de Shelley Lake datant de 1988 intitulée Polly Gone. Elle ranime le personnage de ce dernier dans le but d'aborder le "Digital Sublime" et son côté horrifiant.
  - title: "Neo-Surreal"
    format: "2017 - Impressions numériques "
    meta:  "Courtesy de l’artiste"
    desc: >-
      Une mise en regard contemporaine de publicités pour des produits informatiques datant des années 1980. On y distingue des sujets actuels de la technologie tels que la surveillance, en 2019 ces accents visionnaires ont de quoi vous donner froid dans le dos.
---