---
image: "/assets/images/grrrls/mary-maggic.gif"
artist: "Mary Maggic"
bio: "Mary Maggic est une artiste non-binaire qui travaille à l’interface entre biotechnologie, discours culturel et désobéissance civile. Ses projets les plus récents consistent en des instructions DIY d’extraction et d’identification d’hormones, de micro-performativité, et de hacking du genre."
website: "maggic.ooo"
artwork:
  - title: "Housewives Making Drugs"
    format: "2017 - Vidéo, 10:10 min, poster"
    meta: "Courtesy de l’artiste"
    desc: >-
      Maria et Maria, les deux présentatrice d’une pseudo émission culinaire, nous poussent à reprendre le contrôle de notre corps. Elles court-circuitent le complexe pharmaco-industriel et proposent des méthodes de biohacking pour obtenir des oestrogènes, ceci dans le but d’entamer le processus de changement de sexe, d’un homme à une femme.
---