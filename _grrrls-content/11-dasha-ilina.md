---
image: "/assets/images/grrrls/dasha-ilina.gif"
artist: "Dasha Ilina"
bio: "Dasha Ilina est une artiste numérique de Moscou qui vit à Paris. Son travail questionne les relations que nous entretenons avec nos outils et leurs interfaces, et transforme les réponses en œuvres numériques ou physiques, interactives, éducatives et souvent également ironiques."
website: "dashailina.com"
artwork:
  - title: "Center for Technological Pain"
    format: "2018 - Installation, vidéo, brochures, prototypes"
    meta: "Courtesy de l’artiste"
    desc: >-
      Artiste appartenant à la génération des millenials, accro à Internet et aux écrans, Dasha Ilina nous livre, avec un regard satirique, des conseils DIY et open source pour lutter contre les méfaits que la technologie peut avoir sur notre corps et notre esprit.
---