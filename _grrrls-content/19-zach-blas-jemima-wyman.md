---
image: "/assets/images/grrrls/zach-jemima.gif"
artist: "Zach Blas & Jemima Wyman"
bio: "Zach Blas est artiste, cinéaste, et chercheur au Departement of Visual Cultures, Goldsmiths London University. Il est l’un des lauréats du prix Creative Capital de 2016 dans la catégorie Emerging Fields et Arts and Humanities Research Council Leadership Fellow. Jemima Wyman est une artiste interdisciplinaire qui vit et travaille à Brisbane et Los Angeles. Ses œuvres les plus récentes se concentrent sur les stratégies visuelles de résistance employées dans la culture de protestation et les zones de conflit. "
website: "www.zachblas.info & www.jemimawyman.wordpress.com"
artwork:
  - title: "I’m here to learn so :))))))"
    format: "2017 - Installation vidéo 4 canaux, 27:33 min"
    meta: "Courtesy des artistes"
    desc: >-
      Dans leur installation vidéo, Zach Blas & Jemima Wyman réaniment Tay, l’intelligence artificielle activée par Microsoft sur Twitter en 2016 qui apprenait et évoluait en fonction de ses interactions avec le public. Devenue raciste, sexiste et homophobe en quelques heures par l’action de trolls, elle avait été très vite déconnectée. Ici elle digresse, philosophe et s’amuse de sa condition de chatbot.
---