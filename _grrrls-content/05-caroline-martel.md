---
image: "/assets/images/grrrls/caroline-martel.gif"
artist: "Caroline Martel"
bio: "Caroline Martel est une réalisatrice, artiste et chercheuse canadienne qui s’intéresse tout particulièrement aux archives, aux histoires invisibles, aux technologies audiovisuelles et à l’héritage culturel. Elle a réalisé, entre autres, Le chant des Ondes (2012) et Spectacles du monde (2017), une installation de 35 écrans pour le Musée d’art de Montréal."
artwork:
  - title: "Le fantôme de l’opératrice"
    format: "2004 - Documentaire, 65:00 min"
    meta: "Courtesy des Productions Artifact"
    desc: >-
      À la croisée de la science et de la fiction, ce film montage utilisant des images d’archives rares retrace l’histoire des opératrices téléphoniques, une invisible armée de femmes au service du progrès technologique bientôt remplacée par la machine.
---