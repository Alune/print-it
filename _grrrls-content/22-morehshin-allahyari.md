---
image: "/assets/images/grrrls/morehshin.gif"
artist: "Morehshin Allahyari"
bio: "Morehshin Allahyari, est un artiste et activiste née en 1985 en Iran et qui vit aux États-Unis depuis 2007. Elle s’est fait connaître avec Material Speculation : ISIS (2016), des reconstructions en impression 3D d’artefacts culturels détruits par Daesh. Elle est l’autrice avec Daniel Rourke de The 3D Additivist Manifesto et du 3D Additivist Cookbook."
website: "www.morehshin.com"
artwork:
  - title: "She Who Sees the Unknown: Ya’jooj Ma’jooj"
    format: "2017-2018 - Impression 3D en résine synthétique - Vidéo HD, 09:48 min"
  - title: "She Who Sees the Unknown: Aisha Qandisha"
    format: "2018 - Impression 3D en résine synthétique, Vidéo HD, 07:14 min"
    meta: "Courtesy de l’artiste et de Upfor Gallery, Portland, Etats-Unis"
    desc: >-
      She Who Sees the Unknown présente des impressions 3D de figures surnaturelles de femmes (déesses sombres et des djinns) du Moyen-Orient. Allahyari les invoque pour créer des récits magiques et poético-spéculatifs sur les conséquences de la colonisation et d’autres formes d’oppression contemporaines.
---