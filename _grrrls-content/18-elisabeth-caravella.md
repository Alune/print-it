---
image: "/assets/images/grrrls/elisabeth-caravella.gif"
artist: "Elisabeth Caravella"
bio: "Elisabeth Caravella est une réalisatrice française et artiste basée à Paris. Diplômée du Fresnoy, elle a réalisé des films et installations expérimentaux inspirés par la culture web et le cinéma, comme le film Photoshop Coup de fil anonyme (2009) ou plus récemment Krisis (2018), un machinima tournée dans la réalité virtuelle."
website: "www.elisabethcaravella.com"
artwork:
  - title: "Howto³"
    format: "2019 - Tutoriel cinématrographique immersif, adapté du court-métrage Howto (2014) 20:00 min"
    desc: >-
      Dans Howto³, Elisabeth Caravella s’inspire des vidéos tutorielles réalisées par des particuliers sur Internet. La sienne décrit un logiciel étrange, entre outil de traitement de texte et de fabrication d’objets 3D, qui très vite devient incontrôlable et finit par révéler une présence fantomatique qui perturbe le bon déroulement de l’opération. 
---