---
image: "/assets/images/grrrls/suzanne-treister.gif"
artist: "Suzanne Treister"
bio: "Artiste peintre dans les années 1980, Suzanne Treister, basée à Londres, devient une pionnière des arts numériques dans les années 1990. Son alter-ego, la chercheuse Rosalind Brodsky, l’une des rares femmes à voyager dans le temps, joue un rôle important dans une grande partie de ses projets. Son travail se concentre sur les relations entre les nouvelles technologies, la société, les systèmes de croyance alternatifs et les futurs potentiels de l’humanité."
website: "www.suzannetreister.net"
artwork:
  - title: "SURVIVOR (F) et ASICENE"
    format: "2016-2018 - Sélection d’impressions numériques, Papier-peint "
    meta: "Courtesy de l’artiste et des galeries Annely Juda Fine, Londres et P.P.O.W., New York"
    desc: >-
      Disposée sur une mappemonde et parmi des formules mystérieuses, la série SURVIVOR (F) se compose d’images encadrées. Celles-ci mettent en scène des visions futuristes en des temps et des espaces incertains.
---