---
image: "/assets/images/grrrls/simone-c-n.gif"
artist: "Simone C. Niquille"
bio: "Simone C. Niquille est designeuse et chercheuse à Amsterdam. Elle a contribué au pavillon néerlandais à la Biennale d’Architecture de Venise en 2018."
website: "www.technofle.sh"
artwork:
  - title: "The fragility of life"
    format: "2017-2018 - Vidéo, 24:15 min, gonflables"
    desc: >-
      Dans “The fragility of life” Niquille reprend plusieurs personnages issus de différents logiciels et les met en scène dans une vidéo et des figures gonflables. Elle pointe ici les biais racistes et corporels des protocoles de standardisation présents dans les technologies de modélisation 3D.
---