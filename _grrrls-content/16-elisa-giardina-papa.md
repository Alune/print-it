---
image: "/assets/images/grrrls/elisa-giardina.gif"
artist: "Elisa Giardina Papa"
bio: "Elisa Giardina Papa est une artiste italienne dont l’œuvre examine le genre, la sexualité et le travail dans le contexte du capitalisme néolibéral et de la technologie. Elle vit entre la Sicile et New York."
website: "www.elisagiardinapapa.org"
artwork:
  - title: "Technologies of Care"
    format: "2016 - Installation, vidéos"
    meta: "Courtesy de l’artiste"
    desc: >-
      En interviewant des travailleuses offrant des micro-services en ligne allant de l’ASMR au design d’ongles en passant par une relation amoureuse virtuelle ou du coaching de rencontres en ligne, l’artiste livre une enquête ethnographique portant sur le néolibéralisme numérique débridé.
---