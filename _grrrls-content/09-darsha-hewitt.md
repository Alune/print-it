---
image: "/assets/images/grrrls/darsha-hewitt.gif"
artist: "Darsha Hewitt"
bio: "Darsha Hewitt, artiste canadienne installée en Allemagne, s’intéresse à l’archéologie des média et à l’esthétique et aux pratiques DIY. Son œuvre est marquée par une forte critique féministe de la technologie. Elle déconstruit et expérimente avec des objets domestiques obsolètes pour démystifier les systèmes de pouvoir et de contrôle incorporés dans la culture capitaliste."
website: "darsha.org"
artwork:
  - title: "A Side Man 5000 Adventure"
    format: "2015 - 10 vidéos de 5:00 min"
    meta: "Courtesy de l’artiste"
  - title: "Shimmer Generators V.3D"
    format: "2018 - 2 boîtes à rythmes analogiques Wurlitzer Side Man 5000"
    desc: >-
      Cette série de Darsha Hewitt s’articule autour de la première boîte à rythmes commercialisée : le Side Man 5000 qu’elle a restauré. Elle nous emmène ici dans un curieux voyage pédagogique sur la musique, la physique et le bricolage. 
---