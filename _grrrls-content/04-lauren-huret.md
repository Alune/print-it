---
image: "/assets/images/grrrls/lauren-huret.gif"
artist: "Lauren Huret"
bio: "Lauren Huret est une artiste franco-suisse, installée à Genève. Son travail porte sur les imaginaires technologiques, notamment ceux liés à l’intelligence artificielle, l’histoire de l’informatique, la circulation des images sur le net. Elle utilise une grande variété de médiums comme la vidéo, le collage numérique, la performance et les livres d’artistes."
website: "www.laurenhuret.com"
artwork:
  - title: "Breaking the Internet"
    format: "2016 - Vidéo HD, 35:00 min"
    meta: "Courtesy of the artist"
    desc: >-
      Dans “Breaking the Internet” Lauren Huret confronte l’avènement du travail automatisé, les voix désincarnées des opératrices de téléphone du début du siècle et la représentation du corps des femmes sur les réseaux sociaux, personnifiés par Kim Kardashian. Une histoire de liens entre le corps, la voix et la machine.
---