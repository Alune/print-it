---
image: "/assets/images/grrrls/louise-drulhe.gif"
artist: "Louise Drulhe"
bio: "Louise Drulhe, née en 1990 à Paris, est designeuse graphique et artiste. Elle construit une recherche théorique et plastique sur la cartographie et la représentation de l’espace d’Internet. Elle envisage la spatialisation comme un outil de compréhension socio-politique de cet espace."
website: "louisedrulhe.fr"
artwork:
  - title: "Atlas critique d’ Internet"
    format: "2015 - Poster"
    meta: "Courtesy de l’artiste"
  - title: "Blockchain une architecture de contrôle"
    format: "2016 - Vidéo, 15:00 min"
    desc: >-
      À la rencontre des mondes physiques et numériques, Louise Drulhe explore des manières de cartographier Internet, un territoire “invisible […] dans lequel nous passons notre temps sans connaître sa forme.” Elle élabore de multiples hypothèses pour comprendre et représenter ce cyberespace indissociable du monde réel, ainsi que sa politique et son économie.
---