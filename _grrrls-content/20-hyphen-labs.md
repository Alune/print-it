---
image: "/assets/images/grrrls/hyphen-labs.gif"
artist: "Hyphen-Labs"
bio: "Hyphen-Labs est un groupe international de femmes de couleur, ingénieures, scientifiques, architectes et artistes qui travaillent à l’intersection de l’art et des technologies émergentes, de la science et du futur. Parmi ses co-fondatrices, Ashley Baccus-Clark, biologiste et artiste basée à New York qui explore des thèmes comme la cognition, la mémoire, la race, le trauma et les systèmes de croyance. Ece Tankal, designeuse née en Turquie, architecte et artiste, qui travaille avec la réalité virtuelle et le design spéculatif. Carmen Aguilar y Wedge, designeuse et artiste créant des expériences immersives et transmédia."
website: "www.hyphen-labs.com"
artwork:
  - title: "NeuroSpeculative AfroFeminism"
    format: "2017 - Installation VR, prototypes, vidéo, 4:00 min"
    meta: "Courtesy des artistes"
    desc: >-
      Cette installation en réalité virtuelle vous plonge dans l’univers d’un salon de coiffure futuriste où vous incarnez une femme noire sur le point de se faire poser des « électrodes Octavia », qui propulsent  celui  qui  les  revêt  dans  un  monde  virtuel  onirique. Une expérience à la croisée de l’afro-féminisme et de la science-fiction.
---