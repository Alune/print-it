---
image: "/assets/images/grrrls/nadja-b.gif"
artist: "Nadja Buttendorf"
bio: "Nadja Buttendorf est née en 1984 à Dresde (en ex-Allemagne de l’Est). Ses vidéos, performances, installations et objets adressent les questions sociales autour des images du corps et les stéréotypes dans les rôles de genre. "
website: "nadjabuttendorf.com"
artwork:
  - title: "Robotron - a tech opera"
    format: "2018-2019 - Série web"
    desc: >-
      Cette série YouTube où Nadja rejoue son histoire familiale, est une œuvre à la croisée improbable des genres. Elle a pour cadre Robotron, la plus grande entreprise de fabrication d’ordinateurs d’Allemagne de l'Est. Robotron oscille entre Star Trek, le soap opera allemand et l’esthétique Snapchat, où l’artiste  interprète tous les rôles sur fond vert. Entre la réunification de l'Allemagne et le divorce de ses parents, Nadja livre un étonnant spectacle numérique au accents rétro-futuristes.
  - title: "Soft Nails ♥ [ASMR] Kleincomputer Robotron KC87 ♥"
    format: "2018 - Vidéo HD, son, 13:14 min"
    desc: >-
      L'Autonomous Sensory Meridian Response ou ASMR, fait fureur sur internet au 21e siècle. Nadja Buttendorf se l'approprie dans cette œuvre et nous emmène à nouveau dans le passé grâce au Robotron KC87, un ordinateur conçu en ex-Allemagne de l’Est datant de 1987. Elle détourne ici les vidéos d'ASMR à la pointe de la technologie, pour en faire une version Robotron 87. Un régal pour tes yeux et tes oreilles.

---