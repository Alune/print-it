---
image: "/assets/images/grrrls/jennifer-chan.gif"
artist: "Jennifer Chan"
bio: "Jennifer Chan est une artiste et curatrice canadienne qui a grandi à Hong Kong et vit à Toronto. Son travail explore la représentation de la masculinité et les constructions de genre et de race, notamment dans le champ de l’art numérique et en ligne. Elle utilise souvent l’esthétique amateur inspirée par la culture pop et les mash-up sur YouTube."
website: "jennifer-chan.com"
artwork:
  - title: "*A total Jizzfest*"
    format: "2012 - Vidéo, 3:22 min, cartons à pizza"
    desc: >-
      Dans une vidéo sarcastique à l'esthétique parodiant l’univers des start-up, l’artiste célèbre la Silicon Valley et ses gourous exclusivement masculins dominant l’économie informatique.
---