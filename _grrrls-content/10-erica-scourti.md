---
image: "/assets/images/grrrls/erica-scourti.gif"
artist: "Erica Scourti"
bio: "Le travail d’Erica Scourti peut être interprété comme une autobiographie performative, avec laquelle elle explore les identités à l’intérieur des systèmes bio-sociotechnologiques contemporains. Elle vit entre Athènes et Londres. "
website: "www.ericascourti.com"
artwork:
  - title: "Body Scan"
    format: "2014 - Vidéo, 5:03 min"
    meta: "Courtesy de l’artiste"
    desc: >-
      Un écran aux proportions d’un smartphone diffuse des images d’un corps, celui de l’artiste, soumis à différents moteurs de recherche faisant le lien avec des contenus web. Les résultats, parfois amusants mais le plus souvent sexistes, sont explicités un à un en direct par l’artiste en voix-off. Une manière d'interroger les algorithmes/logiciels de recherche d’images où le corps féminin devient un objet standardisé et marchandisé. 
---