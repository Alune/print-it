---
image: "/assets/images/grrrls/roberte-la-rousse.gif"
artist: "Roberte la Rousse"
bio: "Roberte la Rousse est une collective qui développe des projets artistiques et critiques, fondée par Cécile Babiole, plasticienne, et Anne Laforet, chercheuse. La collective travaille sur la thème « langue française et genre ». Elle s’agit de contrer la sexisme inscrite à la cœur de la langue française."
website: "robertelarousse.fr"
artwork:
  - title: "Wikifémia - la réseau des computer grrrls"
    format: "2019 - Installation interactive"
    desc: >-
      Mise en avant et traduction à la féminine d’articles présent sur l’encyclopédie en ligne  Wikipédia. Les articles choisis sont des biographies de femmes marquantes pour l’histoire de l’informatique.
---