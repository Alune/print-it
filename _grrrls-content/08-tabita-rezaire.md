---
image: "/assets/images/grrrls/tabita-rezaire.gif"
artist: "Tabita Rezaire"
bio: "Tabita Rezaire est une artiste qui travaille avec les écrans et les flux d’énergie. En naviguant dans les architectures de pouvoir, elle révèle les imaginaires scientifiques et adresse la matrice omniprésente du colonialisme. Elle propose une lecture alternative aux récits dominants avec la volonté de démanteler nos écrans « blancs-suprémacistes-patriarcaux-cis-hétéro-globalisés » pour nous (re)connecter. Elle réside en Guyane."
website: "www.tabitarezaire.com"
artwork:
  - title: "Premium Connect"
    format: "2017 - Vidéo HD, 13:04 min"
    meta: "Courtesy de l’artiste et de la galerie Goodman Gallery (Afrique du Sud)"
    desc: >-
      Dans cette vidéo, Tabita Rezaire entreprend de décoloniser le cyberespace et réhabilite les savoirs et cultures indigènes. Elle transforme les flux unilatéraux du réseau et ouvre à d’autres formes de connexion, où mondes organiques, technologiques et spirituels s’entrelacent.
---