---
image: "/assets/images/grrrls/manetta-berends.gif"
artist: "Manetta Berends"
bio: "Manetta Berends, née en 1989 aux Pays-Bas est designer graphique, avec un intérêt pour les infrastructures numériques et le logiciel libre. Elle est membre du collectif Varia, qui développe des approches collectives aux technologies quotidiennes et développe des outils libres à Rotterdam."
website: "http://213.167.241.137/~mb"
artwork:
  - title: "Cyber/technofeminist cross-readings"
    format: "2019 - Outil de recherche"
    meta: "Commissionné par Marie Lechner et Inke Arns"
    desc: >-
      “The Cyber/Technofeminist cross-reader” questionne la place des femmes dans les environnements technologiques. Manetta Berends a développé un moteur de recherche pour naviguer dans une collection de manifestes techno-féministes, basé sur l’algorithme TD-IDF permettant de pondérer les résultats de recherche.
---