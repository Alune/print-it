---
image: "/assets/images/grrrls/lu-yang.gif"
artist: "Lu Yang"
bio: "Lu Yang est une artiste basée à Shanghai, qui travaille avec la vidéo, l’installation, l’animation, la peinture numérique et les jeux. Elle examine les conditions biologiques et matérielles de l’existence humaine avec une dose d’humour noir et l’absence de tout sentimentalisme."
website: "www.luyang.asia"
artwork:
  - title: "Delusional Mandala"
    format: "2015 - Vidéo, 16:27 min"
    meta: "Courtesy de l’artiste"
    desc: >-
      En s’inspirant de la figure du mandala, outil hindouiste ou bouddhiste de méditation et d’introspection représentant un fragment du cosmos et incarnant l’univers, Lu Yang met en scène de sa naissance à sa mort son alter ego Uterus Man, corps numérique traversant une épopée frénétique et délirante.
---