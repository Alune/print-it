---
image: "/assets/images/grrrls/aleksandra-d.gif"
artist: "Aleksandra Domanović"
bio: "Aleksandra Domanović est née en 1981 à Novi-Sad (en ex-Yougoslavie). Ses œuvres prennent souvent ancrage dans le territoire d’ex-Yougoslavie et explorent l’histoire et le développement des technologies et notamment au rôle que les femmes y ont joué."
artwork:
  - title: "VUKOSAVA"
    format: "2013 - Impression 3D par frittage laser, polyuréthane, finition Soft-Touch & laiton, verre acrylique"
    meta: "Courtesy de l’artiste et de la galerie Tanya Leighton (Berlin)"
    desc: >-
      Avec cette sculpture réalisée en impression 3D faisant partie d’une série représentant des gestes et des symboles de différentes traditions culturelles, Aleksandra Domanovic se penche ici sur la “main de Belgrade”, première main artificielle articulée au Monde.
---